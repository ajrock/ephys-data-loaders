function [fig] = radioList(list)
%radioSelection Brings up menua to do file selection for manual control of
%current process

%fig = uifigure('DeleteFcn',@(fig, event) myCloseReq(fig));
a = length(list);
%fig = uifigure('Position',[0 0 560 a*25+200]);
fig = uifigure('Position',[10 10 560 560]);
maxPos = fig.Position(4);
select = [];
% Remove .src from pertenant files
list(contains(list,'.src')) = cellfun(@(x) x(1:end-4),list(contains(list,'.src')),'UniformOutput',false);
guidata(fig,list);
pan = uibuttongroup(fig,'Position',[50 75 500 500]);
pan.Scrollable = 'on';
okBtn = uibutton(fig,'Position',[50 50 100 22],'Text','Ok','ButtonPushedFcn', @(okBtn,event) buttonPsh(okBtn,fig,pan));
tlabel = uilabel(fig,'Text',list{1},'Position',[50 okBtn.Position(2)-25 560 15]);
for ii = 2:min(a,100)
    if length(list{ii})>4 && strcmp(list{ii}(end-3:end),list{1}(end-3:end)) && strcmp(list{ii}(1:15),list{1}(1:15))
        %Pre check boxes that begin and end the same (i.e., same pen,
        %depth, and channel
        expression = sprintf('cb%d = uiradiobutton(pan, ''Text'',''%s'',''Position'',[50 %d 1000 15],''value'',1);',...
        ii-1,list{ii},maxPos-100*(ii));
    else
        expression = sprintf('cb%d = uiradiobutton(pan, ''Text'',''%s'',''Position'',[50 %d 1000 15],''value'',0);',...
        ii-1,list{ii},maxPos-100*(ii));
    end
    eval(expression)
end
uiwait(fig)
end

function select = buttonPsh(okBtn,fig,pan)
    select = pan.SelectedObject.Text;
    guidata(fig,select)
    %closereq();   
    uiresume(fig)
end
% 
% function myCloseReq(fig)
%     uiresume
%     delete(fig);
% end