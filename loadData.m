function tab = loadData(inputFolder,analysisType,loadFunc,tableFile)
%loadData(inputFolder,analysisType,loadFunc,tableFile) 
%this function assembles distributed ephys data sets and extracts
%selected parameters from them. Can run either with all inputs on the
%command line, or you can use a GUI interface to add them one by one.
%   INPUTS:
%       inputFolder: Directory pointing to a folder containing all your
%       data organized as Parent/Animal/IO,ITD,etc/P01....mat. Will not
%       work otherwise! Can be entered as string.
%       analysisType: either IO, ITD, RRTF, or TC. Can be entered as
%       string.
%       loadFunc: a single String or cellvector of strings containing the
%       loaders you wish to use. Alternate use: enter 'all' to
%       automatically use all available loaders for the desired analysis
%       type. Be sure to close the .m files first, or it will error due to
%       finding the .asv file as well!
%       tableFile: directory and filename where you want your excel sheet
%       to be written to. Do NOT have a file open under the same name!
%   OUTPUTS:
%       tab: the matlab table version of the excel output sheet
if ~exist('inputFolder','var')
    inputFolder = uigetdir('C:\Users\curran\Documents');
    if isnumeric(inputFolder)
        tab = [];
        return
    end
end
if ~exist('analysisType','var')
    fig = radioList({'Choose One','IO','ITD','TC','RRTF'});
    analysisType = guidata(fig);
    close(fig)
end
if exist('loadFunc','var') && ischar(loadFunc) && strcmp(loadFunc,'all')
   d = dir(fullfile('loaders',[analysisType '*']));
   loadFunc = {d.name};
end
if ~exist('loadFunc','var') || isempty(loadFunc) || ~iscell(loadFunc)
    thisDir = dir('loaders');
    if isempty(thisDir)
        thisDir = dir(['dataLoader' filesep 'loaders']);
    end
    thisDir(startsWith({thisDir.name},'.'))=[];
    thisDir(~contains({thisDir.name},analysisType,'IgnoreCase',1))=[];
    fig = listSelect({'Choose the analyses you want',...
        thisDir.name});
    loadFunc = guidata(fig);
    close(fig)
end
d=dir([inputFolder '\*\' analysisType '\folderData.mat']);
% Initialize Table
varNames = {'ID','filename','Penetration', 'Depth','Channel','Hemisphere','StimType'};%,'LeftLevel','RightLevel'};
varTypes = {'string','string','double', 'double', 'double','string','string'};%,'double','double'};
tab = table('Size',[2000 length(varNames)],'VariableTypes',varTypes,'VariableNames',varNames);
if ischar(loadFunc)
    dataVect = cell(2000,1);
    loadFunc = erase(loadFunc,'.m');
else
    dataVect = cell(2000,length(loadFunc));
    loadFunc = cellfun(@(x) erase(x,'.m'),loadFunc,'UniformOutput',0);
end
inc = 1;
for ii = 1:length(d) 
    % Inner loop here is per animal.
    loadThisFile = [d(ii).folder '\' d(ii).name];
    load(loadThisFile) % file name called matFile
    fileInds = find(contains(matFile.analysisUsed,'yes'));
    for jj = 1:length(fileInds)
        thisFile = matFile.fileName{fileInds(jj)}(1:end-4);
        if exist([d(ii).folder '\' thisFile '.mat'],'file')
            load([d(ii).folder '\' thisFile '.mat'])
            dataAdd = parseName(d(ii).folder,varNames,varTypes,thisFile,data);
            tab(inc,:) = dataAdd;
            if ischar(loadFunc)
                dataVect{inc,1} = eval([loadFunc '(data)']);
            else
                for kk = 1:length(loadFunc)
                    dataVect{inc,kk} = eval([loadFunc{kk} '(data)']);
                end
            end
            inc = inc+1;
            clear data
        end
    end
end
dataVect(ismissing(tab.ID),:) = [];
tab(ismissing(tab.ID),:)=[];
if ischar(loadFunc)
    tab = [tab cell2table(dataVect,'VariableNames',{loadFunc})];
else
    tab = [tab cell2table(dataVect,'VariableNames',loadFunc)];
end
if ~exist('tableFile','var')
    fprintf('Enter the path to save your data.\n');
    [fileName, fileFolder] = uiputfile('.xlsx');
    tableFile = [fileFolder fileName];
end
if exist(tableFile,'file');delete(tableFile);end
writetable(tab,tableFile);
end



function dataAdd = parseName(folder,varNames,varTypes,thisFile,data)
dataAdd = table('Size',[1 length(varNames)],'VariableTypes',varTypes,'VariableNames',varNames);
foldSplit = regexp(folder,filesep,'split');
foldName = regexp(foldSplit{end-1},' ','split');
dataAdd.ID = string(foldName{end});
dataAdd.Penetration = iFindNumbersInString(thisFile,'substring','P');
dataAdd.Depth = iFindNumbersInString(thisFile,'substring','D');
dataAdd.Channel = iFindNumbersInString(thisFile,'startPos',length(thisFile)-2);
dataAdd.filename=thisFile;
if isfield(data,'stimMux')
    dataAdd.StimType = data.stimMux;
elseif isfield(data,'stimType')
    dataAdd.StimType = data.stimType;
end
% dataAdd.LeftLevel = data.stimLevel(1);
% if length(data.stimLevel)==2
%     dataAdd.RightLevel = data.stimLevel(2);
% else
%     dataAdd.RightLevel = data.stimLevel(1);
% end
if strcmp(dataAdd.ID,"MV")
    dataAdd.Channel = 1;
end
assert(~isnan(dataAdd.Channel),'Bad Channel Read')
dataAdd.Hemisphere = string(data.hemisphere);


end