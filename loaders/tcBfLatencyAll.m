function answer = tcBfLatencyAll(data)
if isfield(data.contra,'bfLatencyStats') && isfield(data.contra.bfLatencyStats,'meanFirstLat')
    answer(1)= data.contra.bfLatencyStats.meanFirstLat;
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'bfLatencyStats') && isfield(data.ipsi.bfLatencyStats,'meanFirstLat')
    answer(2)= data.ipsi.bfLatencyStats.meanFirstLat;
else
    answer(2) = NaN;
end

if isfield(data.bds,'bfLatencyStats') && isfield(data.bds.bfLatencyStats,'meanFirstLat')
    answer(3) = data.bds.bfLatencyStats.meanFirstLat;
else
    answer(3) = NaN;
end

end