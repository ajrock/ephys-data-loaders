function answer = itdSusStvr(data)

if (data.nSweeps)< 18
    % if <18, then use pooled data to obtain more reps
    if isfield(data,'spkCount')
        [~,stat] = anova1(squeeze(sum(data.spkCount(2:end,1:min(size(data.spkCount,2),12),1:2:end),1)),[],'off');
        answer = stat{2,2}/stat{4,2};
    else
        answer = NaN;
    end
else
    % if NOT <18, then use unpooled data
    if isfield(data,'spkCount')
        [~,stat] = anova1(squeeze(sum(data.spkCount(2:end,1:min(size(data.spkCount,2),12),:),1)),[],'off');
        answer = stat{2,2}/stat{4,2};
    else
        answer = NaN;
    end
     
end
