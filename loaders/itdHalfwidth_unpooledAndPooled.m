function answer = itdHalfwidth_unpooledAndPooled(data)
% Organized as: unpooled, pooled
if isfield(data.allPulsesFit,'FIT') && isfield(data.allPulsesFit.FIT,'halfwidth')
    answer(1) = data.allPulsesFit.FIT.halfwidth;
else
    answer(1) = NaN;
end

if isfield(data.allPulsesPooledFit,'FIT') && isfield(data.allPulsesPooledFit.FIT,'halfwidth')
    answer(2) = data.allPulsesPooledFit.FIT.halfwidth;
else
    answer(2) = NaN;
end

end
