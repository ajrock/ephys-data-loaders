function answer = itd0Ndt_D1(data)
if isfield(data.allPulsesPooledFit,'ndt')
    answer = data.allPulsesPooledFit.ndt.classicNDT.one.itd0;
else
    answer = NaN;
end

end