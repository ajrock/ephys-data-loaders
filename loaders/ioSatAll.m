function answer = ioSatAll(data)
if isfield(data.contra,'saturation')
    answer(1)= data.contra.saturation;
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'saturation')
    answer(2)= data.ipsi.saturation;
else
    answer(2) = NaN;
end

if isfield(data.bds,'saturation')
    answer(3) = data.bds.saturation;
else
    answer(3) = NaN;
end

end