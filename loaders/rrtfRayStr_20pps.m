function answer = rrtfRayStr_20pps(data)
if ~isempty(data.contra.stimIndices)
    [~,minIdx]=min(abs(data.contra.rates-20));
    answer(1) = data.contra.filterCharacteristics.RayStrength.data(minIdx);
else
    answer(1) = NaN;
end

if ~isempty(data.ipsi.stimIndices)
    [~,minIdx]=min(abs(data.ipsi.rates-20));
    answer(2) = data.ipsi.filterCharacteristics.RayStrength.data(minIdx);
else
    answer(2) = NaN;
end

if ~isempty(data.bds.stimIndices)
    [~,minIdx]=min(abs(data.bds.rates-20));
    answer(3) = data.bds.filterCharacteristics.RayStrength.data(minIdx);
else
    answer(3) = NaN;
end

end



