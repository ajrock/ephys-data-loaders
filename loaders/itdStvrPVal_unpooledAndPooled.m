function answer = itdStvrPVal_unpooledAndPooled(data)

if isfield(data.allPulsesFit,'pVal')
    answer(1) = data.allPulsesFit.pVal;
else
    answer(1) = NaN;
    
end

if isfield(data.allPulsesPooledFit,'pVal')
    answer(2) = data.allPulsesPooledFit.pVal;
else
    answer(2) = NaN;
    
end

end
