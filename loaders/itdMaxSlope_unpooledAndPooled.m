function answer = itdMaxSlope_unpooledAndPooled(data)
% Organized as: unpooled, pooled
if isfield(data.allPulsesFit,'FIT') && isfield(data.allPulsesFit.FIT,'maxSlope')
    answer(1) = data.allPulsesFit.FIT.maxSlope;
else
    answer(1) = NaN;
end

if isfield(data.allPulsesPooledFit,'FIT') && isfield(data.allPulsesPooledFit.FIT,'maxSlope')
    answer(2) = data.allPulsesPooledFit.FIT.maxSlope;
else
    answer(2) = NaN;
end

end
