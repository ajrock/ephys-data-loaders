function answer = tcCfPlus20LatencyStdAll(data)
if isfield(data.contra,'cfPlus20LatencyStats') && isfield(data.contra.bfLatencyStats,'stdFirstLat')
    answer(1)= data.contra.cfPlus20LatencyStats.stdFirstLat;
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'cfPlus20LatencyStats') && isfield(data.ipsi.bfLatencyStats,'stdFirstLat')
    answer(2)= data.ipsi.cfPlus20LatencyStats.stdFirstLat;
else
    answer(2) = NaN;
end

if isfield(data.bds,'cfPlus20LatencyStats') && isfield(data.bds.bfLatencyStats,'stdFirstLat')
    answer(3) = data.bds.cfPlus20LatencyStats.stdFirstLat;
else
    answer(3) = NaN;
end


end