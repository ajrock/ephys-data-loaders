function answer = itdPMD(data)
if isfield(data,'spkCountMean')
    scm = data.spkCountMean;
    ind = find(data.itds>-130 & data.itds<130);
    answer = (max(scm(ind))-min(scm(ind)))/(max(scm)-min(scm));
else
    answer = 0;
end

end