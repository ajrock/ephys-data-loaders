function answer = itdStvr_unpooledAndPooled(data)

if isfield(data.allPulsesFit,'limitedStvr')
    answer(1) = data.allPulsesFit.limitedStvr;
else
    answer(1) = NaN;
    
end

if isfield(data.allPulsesPooledFit,'limitedStvr')
    answer(2) = data.allPulsesPooledFit.limitedStvr;
else
    answer(2) = NaN;
    
end

end
