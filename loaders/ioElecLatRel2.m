function answer = ioElecLatRel2(data)

if strcmp(data.stimType,'a')
    answer([1,2,3]) = deal(NaN);
    return
end

if isfield(data.contra,'threshold')
    [~, minIdx] = min(abs(data.contra.levels - (data.contra.threshold+2)));
    if data.contra.levels(minIdx) - (data.contra.threshold+2) < 1
        answer(1) = data.contra.firstLat(minIdx);
    else
        answer(1) = NaN;
    end
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'threshold')
    [~, minIdx] = min(abs(data.ipsi.levels - (data.ipsi.threshold+2)));
    if data.ipsi.levels(minIdx) - (data.ipsi.threshold+2) < 1
        answer(2) = data.ipsi.firstLat(minIdx);
    else
        answer(2) = NaN;
    end
else
    answer(2) = NaN;
end

if isfield(data.bds,'threshold')
    [~, minIdx] = min(abs(data.bds.levels - (data.bds.threshold+2)));
    if data.bds.levels(minIdx) - (data.bds.threshold+2) < 1
        answer(3) = data.bds.firstLat(minIdx);
    else
        answer(3) = NaN;
    end
else
    answer(3) = NaN;
end
