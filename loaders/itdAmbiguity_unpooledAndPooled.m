function answer = itdAmbiguity_unpooledAndPooled(data)
% Organized as: unpooled, pooled
% Defined as the percent of ITDs within the physiological range with
% duplicitous spike counts
rng = -130:0.1:130;
if isfield(data.allPulsesFit,'FIT') && ~strcmp(data.allPulsesFit.FIT,'unclassified')
    y=data.allPulsesFit.FIT.pfit(rng);
    answer(1) = max(sum(y<y(1) & y>y(end))/length(y), sum(y>y(1) & y<y(end))/length(y));
else
    answer(1) = NaN;
end

if isfield(data.allPulsesPooledFit,'FIT') && ~strcmp(data.allPulsesPooledFit.FIT,'unclassified')
    y=data.allPulsesPooledFit.FIT.pfit(rng);
    answer(2) = max(sum(y<y(1) & y>y(end))/length(y), sum(y>y(1) & y<y(end))/length(y));
else
    answer(2) = NaN;
end

end

