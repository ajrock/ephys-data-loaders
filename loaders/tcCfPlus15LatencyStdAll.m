function answer = tcCfPlus15LatencyStdAll(data)
if isfield(data.contra,'cfPlus15LatencyStats') && isfield(data.contra.bfLatencyStats,'stdFirstLast')
    answer(1)= data.contra.cfPlus15LatencyStats.stdFirstLast;
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'cfPlus15LatencyStats') && isfield(data.ipsi.bfLatencyStats,'stdFirstLast')
    answer(2)= data.ipsi.cfPlus15LatencyStats.stdFirstLast;
else
    answer(2) = NaN;
end

if isfield(data.bds,'cfPlus15LatencyStats') && isfield(data.bds.bfLatencyStats,'stdFirstLast')
    answer(3) = data.bds.cfPlus15LatencyStats.stdFirstLast;
else
    answer(3) = NaN;
end


end
