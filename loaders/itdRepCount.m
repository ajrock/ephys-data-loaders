function answer = itdRepCount(data)
% Organized as: unpooled, pooled
if isfield(data,'nSweeps')
    answer = data.nSweeps;
else
   answer = nan; 
end
end
