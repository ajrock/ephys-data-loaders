function answer = itdMS(data)
if isfield(data.allPulsesFit,'FIT') &&...
        isfield(data.allPulsesFit.FIT,'maxSlope')
    answer = data.allPulsesFit.FIT.maxSlope;
else
    answer = NaN;
end

end
