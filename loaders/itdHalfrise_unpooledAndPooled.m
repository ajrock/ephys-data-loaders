function answer = itdHalfrise_unpooledAndPooled(data)
% Organized as: unpooled, pooled
if isfield(data.allPulsesFit,'FIT') && isfield(data.allPulsesFit.FIT,'halfrise')
    answer(1) = data.allPulsesFit.FIT.halfrise;
else
    answer(1) = NaN;
end

if isfield(data.allPulsesPooledFit,'FIT') && isfield(data.allPulsesPooledFit.FIT,'halfrise')
    answer(2) = data.allPulsesPooledFit.FIT.halfrise;
else
    answer(2) = NaN;
end

end
