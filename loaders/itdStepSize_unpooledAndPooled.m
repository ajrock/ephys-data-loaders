function answer = itdStepSize_unpooledAndPooled(data)
% Organized as: unpooled, pooled
if isfield(data,'itds')
    answer(1) = diff(data.itds(1:2));
    answer(2)=answer(1)*2;
else
   answer = repelem(nan,1,2); 
end
end
