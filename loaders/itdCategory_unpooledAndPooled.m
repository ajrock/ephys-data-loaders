function answer = itdCategory_unpooledAndPooled(data)
% Organized as: unpooled, pooled
if isfield(data.allPulsesFit,'FIT') 
    answer(1) = string(data.allPulsesFit.FIT.type);
else
    answer(1) = string("");
end

if isfield(data.allPulsesPooledFit,'FIT') 
    answer(2) = string(data.allPulsesPooledFit.FIT.type);
else
    answer(2) = string("");
end

end

