function answer = itdNdtMaxSlope_unpooledAndPooled(data)
% Organized as: unpooled, pooled
if isfield(data.allPulsesFit,'ndt')
    answer(1) = data.allPulsesFit.ndt.classicNDT.one.maxslope;
else
    answer(1) = NaN;
end


if isfield(data.allPulsesPooledFit,'ndt')
    answer(2) = data.allPulsesPooledFit.ndt.classicNDT.one.maxslope;
else
    answer(2) = NaN;
end
