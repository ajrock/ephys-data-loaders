function answer = tcAuralDominance(data)
if isfield(data,'auralDom')
    answer(1)= data.auralDom.RFO;
else
    answer(1) = NaN;
end
