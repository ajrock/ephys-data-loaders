function answer = itdBest_unpooledAndPooled(data)
% Organized as: unpooled, pooled
if isfield(data.allPulsesFit,'FIT') && isfield(data.allPulsesFit.FIT,'itdBest')
    answer(1) = data.allPulsesFit.FIT.itdBest;
else
    answer(1) = NaN;
end

if isfield(data.allPulsesPooledFit,'FIT') && isfield(data.allPulsesPooledFit.FIT,'itdBest')
    answer(2) = data.allPulsesPooledFit.FIT.itdBest;
else
    answer(2) = NaN;
end

end

