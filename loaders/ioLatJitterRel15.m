function answer = ioLatJitterRel15(data)

if strcmp(data.stimType,'e')
    answer([1,2,3]) = deal(NaN);
    return
end

if isfield(data.contra,'threshold')
    [~, minIdx] = min(abs(data.contra.levels - (data.contra.threshold+15)));
    if data.contra.levels(minIdx) - (data.contra.threshold+15) < 5
        answer(1) = data.contra.firstLatStd(minIdx);
    else
        answer(1) = NaN;
    end
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'threshold')
    [~, minIdx] = min(abs(data.ipsi.levels - (data.ipsi.threshold+15)));
    if data.ipsi.levels(minIdx) - (data.ipsi.threshold+15) < 5
        answer(2) = data.ipsi.firstLatStd(minIdx);
    else
        answer(2) = NaN;
    end
else
    answer(2) = NaN;
end

if isfield(data.bds,'threshold')
    [~, minIdx] = min(abs(data.bds.levels - (data.bds.threshold+15)));
    if data.bds.levels(minIdx) - (data.bds.threshold+15) < 5
        answer(3) = data.bds.firstLatStd(minIdx);
    else
        answer(3) = NaN;
    end
else
    answer(3) = NaN;
end
