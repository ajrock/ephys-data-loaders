function answer = rrtfVectorLength_MaxValueRate(data)
if ~isempty(data.contra.stimIndices)
    answer(1) = data.contra.filterCharacteristics.vectLength.maxValueRate;
else
    answer(1) = NaN;
end

if ~isempty(data.ipsi.stimIndices)
    answer(2) = data.ipsi.filterCharacteristics.vectLength.maxValueRate;
else
    answer(2) = NaN;
end

if ~isempty(data.bds.stimIndices)
    answer(3) = data.bds.filterCharacteristics.vectLength.maxValueRate;
else
    answer(3) = NaN;
end

end



