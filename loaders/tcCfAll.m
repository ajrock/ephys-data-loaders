function answer = tcCfAll(data)
if isfield(data.contra,'CF')
    answer(1)= data.contra.CF;
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'CF')
    answer(2)= data.ipsi.CF;
else
    answer(2) = NaN;
end

if isfield(data.bds,'CF')
    answer(3) = data.bds.CF;
else
    answer(3) = NaN;
end

end