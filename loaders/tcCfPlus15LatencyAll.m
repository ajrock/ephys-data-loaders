function answer = tcCfPlus15LatencyAll(data)
if isfield(data.contra,'cfPlus15LatencyStats') && isfield(data.contra.bfLatencyStats,'meanFirstLat')
    answer(1)= data.contra.cfPlus15LatencyStats.meanFirstLat;
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'cfPlus15LatencyStats') && isfield(data.ipsi.bfLatencyStats,'meanFirstLat')
    answer(2)= data.ipsi.cfPlus15LatencyStats.meanFirstLat;
else
    answer(2) = NaN;
end

if isfield(data.bds,'cfPlus15LatencyStats') && isfield(data.bds.bfLatencyStats,'meanFirstLat')
    answer(3) = data.bds.cfPlus15LatencyStats.meanFirstLat;
else
    answer(3) = NaN;
end

end