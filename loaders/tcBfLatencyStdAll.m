function answer = tcBfLatencyStdAll(data)
if isfield(data.contra,'bfLatencyStats') && isfield(data.contra.bfLatencyStats,'stdFirstLast')
    answer(1)= data.contra.bfLatencyStats.stdFirstLast;
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'bfLatencyStats') && isfield(data.ipsi.bfLatencyStats,'stdFirstLast')
    answer(2)= data.ipsi.bfLatencyStats.stdFirstLast;
else
    answer(2) = NaN;
end

if isfield(data.bds,'bfLatencyStats') && isfield(data.bds.bfLatencyStats,'stdFirstLast')
    answer(3) = data.bds.bfLatencyStats.stdFirstLast;
else
    answer(3) = NaN;
end


end