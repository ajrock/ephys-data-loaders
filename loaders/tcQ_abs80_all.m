function answer = tcQ_abs80_all(data)
if isfield(data.contra,'Qfact')
    answer(1)= data.contra.Qfact.cfQabs80;
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'Qfact')
    answer(2)= data.ipsi.Qfact.cfQabs80;
else
    answer(2) = NaN;
end

if isfield(data.bds,'Qfact')
    answer(3) = data.bds.Qfact.cfQabs80;
else
    answer(3) = NaN;
end

end