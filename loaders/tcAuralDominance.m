function answer = tcAuralDominance(data)
if isfield(data,'auralDom')
    answer(1)= data.auralDom.auralDomInd;
else
    answer(1) = NaN;
end
