function answer = ioThrDiff(data)
if isfield(data.contra,'threshold')
    cThr = data.contra.threshold;
else
    cThr = 120;
end

if isfield(data.ipsi,'threshold')
    iThr = data.ipsi.threshold;
else
    iThr = 120;
end

answer = cThr - iThr;

end

