function answer = itdOnsetStvr_unpooledAndPooled(data)

if isfield(data,'spkCount')
    %Unpooled first
    [~,stat] = anova1(squeeze(sum(data.spkCount(1,1:min(size(data.spkCount,2),12),:),1)),[],'off');
    answer(1) = stat{2,2}/stat{4,2};
    % Pooled. 
    
    if rem(length(data.itds),2)==1 %odd number of responses
        midPoint = round(length(data.itds)/2);
        if rem(midPoint,2)==1
            catArray = cat(3,data.spkCount(:,:,2:2:midPoint),...
                data.spkCount(:,:,midPoint),data.spkCount(:,:,midPoint+1:2:end));
        else
            catArray = cat(3,data.spkCount(:,:,2:2:midPoint),...
                data.spkCount(:,:,midPoint+1:2:end));
        end
    else
        catArray = data.spkCount(:,:,2:2:end);
    end
    [~,stat] = anova1(squeeze(sum(catArray(1,1:min(size(catArray,2),12),:),1)),[],'off');
    answer(2) = stat{2,2}/stat{4,2};
else
    answer = repelem(NaN,1,2);
end


end
