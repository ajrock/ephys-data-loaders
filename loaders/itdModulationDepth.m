function answer = itdModulationDepth(data)
% Organized as: unpooled, pooled
% Defined as range(spikes)/max(spikes)
if isfield(data,'spkCountMean')
    answer(1) = range(data.allPulsesFit.spkCountMean)/max(data.allPulsesFit.spkCountMean);
    answer(2) = range(data.allPulsesPooledFit.spkCountMean)/max(data.allPulsesFit.spkCountMean);
else
   answer = repelem(nan,1,2); 
end
end
