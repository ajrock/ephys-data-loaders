function answer = tcBfAll(data)
if isfield(data.contra,'BF')
    answer(1)= data.contra.BF;
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'BF')
    answer(2)= data.ipsi.BF;
else
    answer(2) = NaN;
end

if isfield(data.bds,'BF')
    answer(3) = data.bds.BF;
else
    answer(3) = NaN;
end

end