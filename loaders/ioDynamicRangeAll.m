function answer = ioDynamicRangeAll(data)
if isfield(data.contra,'dynamicRange')
    answer(1)= data.contra.dynamicRange;
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'dynamicRange')
    answer(2)= data.ipsi.dynamicRange;
else
    answer(2) = NaN;
end

if isfield(data.bds,'dynamicRange')
    answer(3) = data.bds.dynamicRange;
else
    answer(3) = NaN;
end

end