function answer = itdLeftDelay(data)

if any(contains(data.paramNames,'leftDelay'))
    answer = unique(data.stimGrid(contains(data.paramNames,'leftDelay'),:));
else
    answer = NaN;
end

end

