
function answer = itdAllStvr(data)
if isfield(data.allPulsesFit,'stvr') 
    answer = data.allPulsesFit.stvr;
else
    answer = NaN;
end

end

