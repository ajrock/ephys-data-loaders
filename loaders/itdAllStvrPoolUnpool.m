function answer = itdAllStvrPoolUnpool(data)

if (data.nSweeps)< 18
    % if <18, then use pooled data to obtain more reps
    if isfield(data.allPulsesPooledFit,'limitedStvr')
        answer = data.allPulsesPooledFit.limitedStvr;
    else
        answer = NaN;
        
    end
else
     % if NOT <18, then use unpooled data
    if isfield(data.allPulsesFit,'limitedStvr')
        answer = data.allPulsesFit.limitedStvr;
    else
        answer = NaN;
        
    end
    
end
