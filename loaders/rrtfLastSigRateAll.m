function answer = rrtfLastSigRateAll(data)
if ~isempty(data.contra.stimIndices)
    ind = find(data.contra.noFirstPulse.PValue>0.01,1);
    if isempty(ind)
        answer(1) = max(data.contra.rates)
    elseif ind == 1
        answer(1) = NaN;
    else
        answer(1)= data.contra.rates(ind-1);
    end
else
    answer(1) = NaN;
end

if ~isempty(data.ipsi.stimIndices)
    ind = find(data.ipsi.noFirstPulse.PValue>0.01,1);
    if isempty(ind)
        answer(2) = max(data.ipsi.rates)
    elseif ind == 1
        answer(2) = NaN;
    else
        answer(2)= data.ipsi.rates(ind-1);
    end
else
    answer(2) = NaN;
end

if ~isempty(data.bds.stimIndices)
    ind = find(data.bds.noFirstPulse.PValue>0.01,1);
    if isempty(ind)
        answer(3) = max(data.bds.rates)
    elseif ind == 1
        answer(3) = NaN;
    else
        answer(3)= data.bds.rates(ind-1);
    end
else
    answer(3) = NaN;
end