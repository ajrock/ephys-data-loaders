function answer = rrtfEntrainment_bandwidth(data)
if ~isempty(data.contra.stimIndices)
    answer(1) = data.contra.filterCharacteristics.entrainment.bandwidth;
else
    answer(1) = NaN;
end

if ~isempty(data.ipsi.stimIndices)
    answer(2) = data.ipsi.filterCharacteristics.entrainment.bandwidth;
else
    answer(2) = NaN;
end

if ~isempty(data.bds.stimIndices)
    answer(3) = data.bds.filterCharacteristics.entrainment.bandwidth;
else
    answer(3) = NaN;
end

end



