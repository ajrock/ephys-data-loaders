function answer = itdSortSlope_unpooledAndPooled(data)
%organized as unpooled - up to three answers (but always 3 returned):
%pooled, up to three answers

answer = repelem(nan,1,6);

if isfield(data.allPulsesFit,'FIT') &&...
        isfield(data.allPulsesFit.FIT,'sizeSortedSlopeMaxima')
    for ii = 1:length(data.allPulsesFit.FIT.sizeSortedSlopeMaxima)
        answer(ii) = data.allPulsesFit.FIT.sizeSortedSlopeMaxima(ii);
    end
end

if isfield(data.allPulsesPooledFit,'FIT') &&...
        isfield(data.allPulsesPooledFit.FIT,'sizeSortedSlopeMaxima')
    for ii = 1:length(data.allPulsesPooledFit.FIT.sizeSortedSlopeMaxima)
        answer(ii+3) = data.allPulsesPooledFit.FIT.sizeSortedSlopeMaxima(ii);
    end
end

end