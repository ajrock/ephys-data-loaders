function answer = ioThrAll(data)
if isfield(data.contra,'threshold')
    answer(1)= data.contra.threshold;
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'threshold')
    answer(2)= data.ipsi.threshold;
else
    answer(2) = NaN;
end

if isfield(data.bds,'threshold')
    answer(3) = data.bds.threshold;
else
    answer(3) = NaN;
end

end