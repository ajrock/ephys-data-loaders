function answer = itdFitWindow_unpooledAndPooled(data)
% Organized as: unpooled, start-stop, pooled, start-stop
if isfield(data.allPulsesFit,'FIT') && isfield(data.allPulsesFit.FIT,'start')
    answer(1) = data.allPulsesFit.FIT.start;
    answer(2) = data.allPulsesFit.FIT.stop;
else
    answer(1) = NaN;
    answer(2) = NaN;
end

if isfield(data.allPulsesPooledFit,'FIT') && isfield(data.allPulsesPooledFit.FIT,'start')
    answer(3) = data.allPulsesPooledFit.FIT.start;
    answer(4) = data.allPulsesPooledFit.FIT.stop;
else
    answer(3) = NaN;
    answer(4) = NaN;
end

end
