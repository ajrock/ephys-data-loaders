function answer = tcCfPlus20LatencyAll(data)
if isfield(data.contra,'cfPlus20LatencyStats') && isfield(data.contra.bfLatencyStats,'meanFirstLat')
    answer(1)= data.contra.cfPlus20LatencyStats.meanFirstLat;
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'cfPlus20LatencyStats') && isfield(data.ipsi.bfLatencyStats,'meanFirstLat')
    answer(2)= data.ipsi.cfPlus20LatencyStats.meanFirstLat;
else
    answer(2) = NaN;
end

if isfield(data.bds,'cfPlus20LatencyStats') && isfield(data.bds.bfLatencyStats,'meanFirstLat')
    answer(3) = data.bds.cfPlus20LatencyStats.meanFirstLat;
else
    answer(3) = NaN;
end

end