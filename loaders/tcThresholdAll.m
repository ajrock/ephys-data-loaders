function answer = tcThresholdAll(data)
if isfield(data.contra,'thr')
    answer(1)= data.contra.thr;
else
    answer(1) = NaN;
end

if isfield(data.ipsi,'thr')
    answer(2)= data.ipsi.thr;
else
    answer(2) = NaN;
end

if isfield(data.bds,'thr')
    answer(3) = data.bds.thr;
else
    answer(3) = NaN;
end

end