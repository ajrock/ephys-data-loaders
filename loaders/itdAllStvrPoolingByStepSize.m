function answer = itdAllStvrPoolingByStepSize(data)

if diff(data.allPulsesFit.itds(1:2))< 100
    % for smaller step sizes, take pooled dat. Assumption that itd step
    % size is constant!
    if isfield(data.allPulsesPooledFit,'limitedStvr')
        answer = data.allPulsesPooledFit.limitedStvr;
    else
        answer = NaN;
    end
else
    if isfield(data.allPulsesFit,'limitedStvr')
        answer = data.allPulsesFit.limitedStvr;
    else
        answer = NaN;
    end
    
end

