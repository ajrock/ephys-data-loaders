function [fig] = listSelect(list)
%radioSelection Brings up menua to do file selection for manual control of
%current process

%fig = uifigure('DeleteFcn',@(fig, event) myCloseReq(fig));
a = length(list);
%fig = uifigure('Position',[0 0 560 a*25+200]);
monitor = get(0,'ScreenSize');
figPos = [50 50 300 min(monitor(4)-100,110+a*25)];
fig = uifigure('Position',figPos);
maxPos = fig.Position(4);
select = [];

guidata(fig,list);
pan = uipanel(fig,'Position',[50 75 figPos(3)-50*2 maxPos-100]);
%pan.Scrollable = 'on';
okBtn = uibutton(fig,'Position',[50 50 100 22],'Text','Ok','ButtonPushedFcn', @(okBtn,event) buttonPsh(okBtn,fig,pan));
tlabel = uilabel(fig,'Text',list{1},'Position',[50 okBtn.Position(2)-25 300 15]);
cb = [];
for ii = 2:min(a,100)
    cb(ii-1) = uicheckbox(pan, 'Text',list{ii},'Position',[20 maxPos-100-25*(ii-1) figPos(3)-50 15],'value',0);
end
uiwait(fig)
end

function select = buttonPsh(okBtn,fig,pan)
    select = [];
    for ii = 1:length(pan.Children)
        if pan.Children(ii).Value
            select = [select, {pan.Children(ii).Text}];
        end
    end
    guidata(fig,select)
    %closereq();   
    uiresume(fig)
end
% 
% function myCloseReq(fig)
%     uiresume
%     delete(fig);
% end