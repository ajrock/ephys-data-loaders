function num = iFindNumbersInString(str,varargin)
%num = iFindNumbersInString(str,startPos)
% takes a starting position and a string (character array) and reads off
% consecutive numerical characters as one number and returns it. Useful for
% extracting numbers from strings automatically
%
% INPUTS:
% str: string to be searched
% varargin: startPos: numerical starting position (if using regexp, its the index +1)
%           substring: string to search inside first
%           substringEnd: string to search inside, but from the right
%
% OUTPUT:
% num: the final number
%
% Uses the char-num relationship to get consecutive digits from 0-9

if strcmp(varargin{1},'startPos')
    startPos = varargin{2};
elseif strcmp(varargin{1},'substring')
    plusInd = length(varargin{2});
    subStrInd = regexp(str,varargin{2});
    subStrInd = subStrInd(1);
    startPos = subStrInd + plusInd;
elseif strcmp(varargin{1},'substringEnd')
    str = fliplr(str);
    plusInd = length(varargin{2});
    subStrInd = regexp(str,fliplr(varargin{2}));
    subStrInd = subStrInd(1);
    startPos = subStrInd + plusInd;
else
    error('Bad input!')
end

%48 is the magic number to represent the character '0' as 0, all the way to
%'9' as 9
strAsDouble = double(str(startPos:end))-48; 

nextInd = find(strAsDouble<0 | strAsDouble>9,1)+startPos-2;

if isempty(nextInd)
    nextInd = length(str);
end

if strcmp(varargin{1},'substringEnd')
    str = fliplr(str);
    oldStart = startPos;
    oldInd = nextInd;
    startPos = length(str)-oldInd+1;
    nextInd = length(str)-oldStart+1;
end

num = str2double(str(startPos:nextInd));
